package io.tedsenft.gemstones.abilities;

import io.tedsenft.gemstones.api.GemActivationType;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

/**
 * Created by Ted on 2/25/2016.
 */
public class AbilityWaterBreathing implements IGemstoneAbility {

    public AbilityWaterBreathing(){ }

    @Override
    public NBTTagCompound getNBT(ItemStack stack) {
        return new NBTTagCompound();
    }

    /**
     * Required for getting an instance of the ability from registration.
     * Also helps if the registry needs to send any queries back to the mod.
     *
     * @return
     */
    @Override
    public String getModID() {
        return Reference.MOD_ID;
    }

    /**
     * This adds "gemstones.abilities." before whatever this returns.
     * It's used to look up a display name for a gemstone ability.
     *
     * @return
     */
    @Override
    public String getUnlocalizedName() {
        return "water_breathing";
    }

    @Override
    public boolean isActive(ItemStack stack) {
        return true;
    }

    @Override
    public void addEffects(ItemStack stack, EntityLivingBase player) { }

    @Override
    public void remEffects(ItemStack stack, EntityLivingBase player) { }

    @Override
    public void handleDamage(LivingAttackEvent source, EntityLivingBase player, ItemStack stack) {
        if(source.getSource() == DamageSource.drown) source.setCanceled(true);
    }

    @Override
    public boolean isActivatedBy(GemActivationType type) {
        return type == GemActivationType.ENTITY_DAMAGED;
    }
}
