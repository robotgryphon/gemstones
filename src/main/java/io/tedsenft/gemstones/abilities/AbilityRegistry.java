package io.tedsenft.gemstones.abilities;

import io.tedsenft.gemstones.api.IGemstoneAbility;

import java.util.HashMap;

public class AbilityRegistry {

  private static AbilityRegistry instance;
  private HashMap<String, IGemstoneAbility> abilityMap;

  private AbilityRegistry(){
    this.abilityMap = new HashMap<>();
  }

  public static void init(){
    if(instance == null)
      instance = new AbilityRegistry();
  }

  public static boolean registerAbility(String modid, Class<? extends IGemstoneAbility> ability){
    try {
      IGemstoneAbility iga = ability.newInstance();

      if(instance.abilityMap.containsKey(modid.toLowerCase() + ":" + iga.getUnlocalizedName().toLowerCase()))
        return false;

      instance.abilityMap.put(modid.toLowerCase() + ":" + iga.getUnlocalizedName().toLowerCase(), iga);
      return true;

    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    return false;
  }

  public static String[] getRegisteredClasses(){
    String[] returned = new String[instance.abilityMap.size()];
    return instance.abilityMap.keySet().toArray(returned);
  }

  public static boolean isAbilityRegistered(String name){
    return instance.abilityMap.containsKey(name.toLowerCase());
  }

  public static IGemstoneAbility getAbilityByMap(String map){
    if(!isAbilityRegistered(map)) return null;
    return instance.abilityMap.get(map.toLowerCase());
  }
}
