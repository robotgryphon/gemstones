package io.tedsenft.gemstones.abilities;

import io.tedsenft.gemstones.api.GemActivationType;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

/**
 * Created by Ted on 2/21/2016.
 */
public class AbilityFlight implements IGemstoneAbility {

    public AbilityFlight(){ }

    @Override
    public NBTTagCompound getNBT(ItemStack stack) {
        return new NBTTagCompound();
    }

    /**
     * Required for getting an instance of the ability from registration.
     * Also helps if the registry needs to send any queries back to the mod.
     *
     * @return
     */
    @Override
    public String getModID() {
        return Reference.MOD_ID;
    }

    /**
     * This adds "gemstones.abilities." before whatever this returns.
     * It's used to look up a display name for a gemstone ability.
     *
     * @return
     */
    @Override
    public String getUnlocalizedName() {
        return "flight";
    }

    /**
     * Used to check if a gem should apply it's effects.
     *
     * @param stack The current itemstack.
     * @return
     */
    @Override
    public boolean isActive(ItemStack stack) {
        return true;
    }

    /**
     * Adds effects to a player.
     * Called as part of a loop when the container item is equipped.
     *
     * @param stack  The gemstone.
     * @param player The player to affect.
     */
    @Override
    public void addEffects(ItemStack stack, EntityLivingBase player) {
        if(player instanceof EntityPlayer) ((EntityPlayer) player).capabilities.allowFlying = true;
    }

    /**
     * Removes effects from a player.
     * Called as part of a loop when the container item is taken off.
     *
     * @param stack  The gemstone.
     * @param player The player to affect.
     */
    @Override
    public void remEffects(ItemStack stack, EntityLivingBase player) {
        if(!(player instanceof EntityPlayer)) return;

        EntityPlayer p = (EntityPlayer) player;
        p.capabilities.allowFlying = false;
        p.capabilities.isFlying = false;
    }

    @Override
    public void handleDamage(LivingAttackEvent source, EntityLivingBase player, ItemStack stack) {
        if(source.getSource() == DamageSource.fall) source.setCanceled(true);
    }

    @Override
    public boolean isActivatedBy(GemActivationType type) {
        return type == GemActivationType.EQUIP || type == GemActivationType.ENTITY_DAMAGED;
    }
}
