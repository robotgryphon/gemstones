package io.tedsenft.gemstones.abilities;

import io.tedsenft.gemstones.api.GemActivationType;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

/**
 * Created by Ted on 2/25/2016.
 */
public class AbilityFireProtection implements IGemstoneAbility {

    public AbilityFireProtection() { }

    @Override
    public NBTTagCompound getNBT(ItemStack stack) {
        return new NBTTagCompound();
    }

    /**
     * Required for getting an instance of the ability from registration.
     * Also helps if the registry needs to send any queries back to the mod.
     *
     * @return
     */
    @Override
    public String getModID() {
        return Reference.MOD_ID;
    }

    /**
     * This adds "gemstones.abilities." before whatever this returns.
     * It's used to look up a display name for a gemstone ability.
     *
     * @return
     */
    @Override
    public String getUnlocalizedName() {
        return "fire_protection";
    }

    @Override
    public boolean isActive(ItemStack stack) {
        return true;
    }

    /**
     * Adds effects to a player.
     * Called as part of a loop when the container item is equipped.
     *
     * @param stack     The gemstone.
     * @param player    The player to affect.
     */
    @Override
    public void addEffects(ItemStack stack, EntityLivingBase player) {
        player.extinguish();
    }

    /**
     * Removes effects from a player.
     * Called as part of a loop when the container item is taken off.
     *
     * @param stack     The gemstone.
     * @param player    The player to affect.
     */
    @Override
    public void remEffects(ItemStack stack, EntityLivingBase player) { }

    @Override
    public void handleDamage(LivingAttackEvent source, EntityLivingBase player, ItemStack stack) {
        if(source.getSource().isFireDamage()){
            source.setCanceled(true);
        }
    }

    @Override
    public boolean isActivatedBy(GemActivationType type) {
        return type == GemActivationType.ENTITY_DAMAGED || type == GemActivationType.TICK;
    }
}
