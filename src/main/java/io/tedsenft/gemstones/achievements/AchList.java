package io.tedsenft.gemstones.achievements;

import net.minecraft.stats.Achievement;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.event.entity.player.AchievementEvent;

/**
 * Created by Ted on 2/28/2016.
 */
public class AchList {

    public static AchievementPage gemstonesPage;

    public static Achievement gettingStarted;
    public static Achievement theBasics;
}
