package io.tedsenft.gemstones.achievements;

import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.items.ItemHammer;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;

public class AchRegistration {

    public static void registerAchievements(){
        AchList.gettingStarted = new Achievement(Reference.MOD_ID + ":achievement.gettingStarted", Reference.MOD_ID + ".getting_started", 0, 0, GemstonesItem.instance(ItemHammer.class), null).registerStat();
        AchList.theBasics = new Achievement(Reference.MOD_ID + ":achievement.theBasics", Reference.MOD_ID + ".the_basics", 2, 0, GemstonesItem.instance(Gemstone.class), AchList.gettingStarted).registerStat();

        AchList.gemstonesPage = new AchievementPage("Gemstones",
                AchList.gettingStarted,
                AchList.theBasics);

        AchievementPage.registerAchievementPage(AchList.gemstonesPage);
    }
}
