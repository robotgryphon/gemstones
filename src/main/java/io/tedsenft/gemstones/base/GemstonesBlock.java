package io.tedsenft.gemstones.base;

import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.creativetabs.TabGemstones;
import io.tedsenft.gemstones.utils.ModelHelper;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.List;

public class GemstonesBlock extends Block implements IWailaDataProvider {

  public GemstonesBlock(Material mat) {
    super(mat);
    setCreativeTab(TabGemstones.instance);
  }

  public static void init(Class<? extends Block> c) {
    try {

      Block ib = c.newInstance();
      GameRegistry.registerBlock(ib);

      Gemstones.registeredStuff.put(c.getName(), ib);
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public static void initRendering(Class<? extends Block> c){
    initRendering(c, "inventory");
  }

  public static void initRendering(Class<? extends Block> c, String variant){
    ModelHelper.registerModel(Item.getItemFromBlock(instance(c)), variant);
  }

  public static Block instance(Class<? extends Block> c){
    String callerName = c.getName();
    return Gemstones.registeredStuff.containsKey(callerName) ? (Block) Gemstones.registeredStuff.get(callerName) : null;
  }

  @Override
  public ItemStack getWailaStack(IWailaDataAccessor data, IWailaConfigHandler config) {
    return data.getStack();
  }

  @Override
  public List<String> getWailaHead(ItemStack itemStack, List<String> list, IWailaDataAccessor data, IWailaConfigHandler config) {
    return list;
  }

  @Override
  public List<String> getWailaBody(ItemStack itemStack, List<String> list, IWailaDataAccessor data, IWailaConfigHandler config) {
    return list;
  }

  @Override
  public List<String> getWailaTail(ItemStack itemStack, List<String> list, IWailaDataAccessor data, IWailaConfigHandler config) {
    return list;
  }

  @Override
  public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity tile, NBTTagCompound tags, World world, BlockPos pos) {
    return tags;
  }
}
