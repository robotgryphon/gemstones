package io.tedsenft.gemstones.base;

import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.creativetabs.TabGemstones;
import io.tedsenft.gemstones.utils.ModelHelper;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;

public class GemstonesItem extends Item {

  public GemstonesItem(){
    setCreativeTab(TabGemstones.instance);
  }

  public static void init(Class<? extends Item> c){
    try {
      Item ib = c.newInstance();
      GameRegistry.registerItem(ib);

      Gemstones.registeredStuff.put(c.getName(), ib);
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public static void initRendering(Class<? extends Item> c){
    initRendering(c, "inventory");
  }

  public static void initRendering(Class<? extends Item> c, String variant){
    ModelHelper.registerModel(instance(c), variant);
  }

  public static Item instance(Class<? extends Item> c){
      String callerName = c.getName();
      return Gemstones.registeredStuff.containsKey(callerName) ? (Item) Gemstones.registeredStuff.get(callerName) : null;
  }
}
