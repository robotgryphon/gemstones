package io.tedsenft.gemstones.base;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.api.GemActivationType;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.creativetabs.TabGemstones;
import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.reference.Guis;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

import java.util.ArrayList;
import java.util.List;


public abstract class GemContainer extends GemstonesItem implements IGemstoneHolder {

    private int slots;

    public GemContainer(int slots) throws IllegalArgumentException {
        if(slots < 1 || slots > 18)
            throw new IllegalArgumentException("Max slot size is 18, minimum is 1.");

        setCreativeTab(TabGemstones.instance);
        setMaxStackSize(1);
        setNoRepair();
        this.slots = slots;
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
        if(stack.getTagCompound() == null)
            stack.setTagCompound(new NBTTagCompound());
    }

    @Override
    public int getNumberSlots(){ return this.slots; }

    public ItemStack[] getGems(ItemStack stack){
        if(!stack.hasTagCompound() || stack.getTagCompound().hasNoTags()) return new ItemStack[0];

        NBTTagCompound nbt = stack.getTagCompound();
        ArrayList<ItemStack> gems = new ArrayList<ItemStack>();
        for(int slot = 0; slot < this.slots; slot++) {
            if (nbt.hasKey("gem_" + slot)) {
                ItemStack gemStack = ItemStack.loadItemStackFromNBT(nbt.getCompoundTag("gem_" + slot));
                gems.add(gemStack);
            }
        }

        ItemStack[] gemsConv = new ItemStack[gems.size()];
        return gems.toArray(gemsConv);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        if(!stack.hasTagCompound()) return;

        ItemStack[] gems = getGems(stack);
        if(gems.length == 0){
            tooltip.add(I18n.translateToLocal(Reference.MOD_ID + ".errors.no_gems_installed"));
            return;
        }

        if(GuiScreen.isShiftKeyDown()) {
            for (int gemSlot = 0; gemSlot < gems.length; gemSlot++) {
                // Look up abilities on gem
                if (gems[gemSlot] == null) continue;
                tooltip.add(ChatFormatting.GREEN + I18n.translateToLocal(gems[gemSlot].getDisplayName()));

                IGemstoneAbility[] gemAbilities = ((Gemstone) gems[gemSlot].getItem()).getAbilities(gems[gemSlot]);
                for (IGemstoneAbility ab : gemAbilities)
                    tooltip.add("  " + ChatFormatting.GRAY + I18n.translateToLocal("gemstones.abilities." + ab.getUnlocalizedName()));
            }
        } else {
            tooltip.add(ChatFormatting.GRAY + I18n.translateToLocal("gemstones.messages.show_gem_abilities"));
        }

    }

    public void handleDamage(ItemStack stack, LivingAttackEvent event){
        ItemStack[] gems = getGems(stack);
        if(gems.length == 0) return;

        for(ItemStack gemStack : gems){
            IGemstoneAbility[] abilities = ((Gemstone) gemStack.getItem()).getAbilities(gemStack);
            for(IGemstoneAbility ability : abilities){
                if(ability.isActivatedBy(GemActivationType.ENTITY_DAMAGED)) ability.handleDamage(event, event.getEntityLiving(), stack);
            }
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand) {
        playerIn.openGui(Gemstones.instance, Guis.GEM_HOLDER, worldIn, 0, 0, 0);
        return new ActionResult<>(EnumActionResult.SUCCESS, itemStackIn);
    }

    @Override
    public void update(ItemStack item, EntityLivingBase holder) {
        ItemStack[] gems = getGems(item);
        if(gems.length == 0) return;

        for(ItemStack gemStack : gems){
            IGemstoneAbility[] abilities = ((Gemstone) gemStack.getItem()).getAbilities(gemStack);
            for(IGemstoneAbility ability : abilities){
                if(ability.isActivatedBy(GemActivationType.TICK)) ability.addEffects(item, holder);
            }
        }
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player) {
        this.onUnequip(item, player);
        return true;
    }

    @Override
    public void onEquip(ItemStack item, EntityLivingBase equipper) {
        ItemStack[] gems = getGems(item);
        if(gems.length == 0) return;

        for(ItemStack gemStack : gems){
            IGemstoneAbility[] abilities = ((Gemstone) gemStack.getItem()).getAbilities(gemStack);
            for(IGemstoneAbility ability : abilities){
                if(ability.isActivatedBy(GemActivationType.EQUIP)) ability.addEffects(item, equipper);
            }
        }
    }

    @Override
    public void onUnequip(ItemStack item, EntityLivingBase unequipper) {
        ItemStack[] gems = getGems(item);
        if(gems.length == 0) return;

        for(ItemStack gemStack : gems){
            IGemstoneAbility[] abilities = ((Gemstone) gemStack.getItem()).getAbilities(gemStack);
            for(IGemstoneAbility ability : abilities){
                if(ability.isActivatedBy(GemActivationType.EQUIP)) ability.remEffects(item, unequipper);
            }
        }
    }

    @Override
    public boolean handleAttacking(ItemStack weapon, EntityLivingBase attacked) {
        return false;
    }
}
