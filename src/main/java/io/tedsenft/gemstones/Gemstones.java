package io.tedsenft.gemstones;

import io.tedsenft.gemstones.achievements.AchRegistration;
import io.tedsenft.gemstones.commands.CommandGems;
import io.tedsenft.gemstones.compat.waila.WailaRegistration;
import io.tedsenft.gemstones.creativetabs.TabGemstones;
import io.tedsenft.gemstones.gui.GuiHandler;
import io.tedsenft.gemstones.creativetabs.TabGems;
import io.tedsenft.gemstones.abilities.AbilityFireProtection;
import io.tedsenft.gemstones.abilities.AbilityFlight;
import io.tedsenft.gemstones.abilities.AbilityRegistry;
import io.tedsenft.gemstones.abilities.AbilityWaterBreathing;
import io.tedsenft.gemstones.messages.MessageRegistry;
import io.tedsenft.gemstones.proxy.CommonProxy;
import io.tedsenft.gemstones.reference.Reference;
import io.tedsenft.gemstones.utils.LogHelper;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

import java.util.HashMap;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.MOD_VERSION, dependencies = Reference.DEPENDENCIES)
public class Gemstones {

    @Mod.Instance(Reference.MOD_ID)
    public static Gemstones instance;

    @SidedProxy(clientSide = Reference.PROXY_PACKAGE + "ClientProxy", serverSide = Reference.PROXY_PACKAGE + "CommonProxy")
    public static CommonProxy proxy;

    public static SimpleNetworkWrapper network;

    public static HashMap<String, Object> registeredStuff;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        TabGemstones.instance = new TabGemstones();
        TabGems.instance = new TabGems();

        registeredStuff = new HashMap<String, Object>();

        proxy.preInit();

        GuiHandler.instance = new GuiHandler();
        network = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);

        MessageRegistry.registerMessages();

        LogHelper.instance = event.getModLog();
        MessageRegistry.registerMessages();

        EventHandler.instance = new EventHandler();
        MinecraftForge.EVENT_BUS.register(EventHandler.instance);

        if(Loader.isModLoaded("Waila")) WailaRegistration.register();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, GuiHandler.instance);
        AchRegistration.registerAchievements();

        LogHelper.log("Activating ability registry and adding built-in abiities..");
        AbilityRegistry.init();
        AbilityRegistry.registerAbility(Reference.MOD_ID, AbilityFireProtection.class);
        AbilityRegistry.registerAbility(Reference.MOD_ID, AbilityFlight.class);
        AbilityRegistry.registerAbility(Reference.MOD_ID, AbilityWaterBreathing.class);
        LogHelper.log("Ability registry activation complete.");

        LogHelper.log("Registering crucible recipes.");
        proxy.registerCrucibleRecipes();
    }

    @Mod.EventHandler
    public void serverStarted(FMLServerStartingEvent ev){
        ev.registerServerCommand(new CommandGems());
    }
}
