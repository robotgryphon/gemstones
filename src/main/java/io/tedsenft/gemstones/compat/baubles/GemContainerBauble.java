package io.tedsenft.gemstones.compat.baubles;

import baubles.api.IBauble;
import io.tedsenft.gemstones.base.GemContainer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Optional;

@Optional.InterfaceList({
        @Optional.Interface(iface = "baubles.api.IBauble", modid = "Baubles"),
        @Optional.Interface(iface = "baubles.api.BaubleType", modid = "Baubles")
})
public abstract class GemContainerBauble extends GemContainer implements IBauble {

  public GemContainerBauble(int slots) throws IllegalArgumentException {
    super(slots);
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public void onWornTick(ItemStack stack, EntityLivingBase ent) {
    this.update(stack, ent);
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public void onEquipped(ItemStack stack, EntityLivingBase ent) {
    this.onEquip(stack, ent);
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public void onUnequipped(ItemStack stack, EntityLivingBase ent) {
    if(ent.worldObj.isRemote) return;
    this.onUnequip(stack, ent);
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public boolean canEquip(ItemStack itemStack, EntityLivingBase ent) {
    return true;
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public boolean canUnequip(ItemStack itemStack, EntityLivingBase ent) {
    return true;
  }

}
