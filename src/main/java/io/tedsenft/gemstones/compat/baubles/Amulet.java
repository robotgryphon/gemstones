package io.tedsenft.gemstones.compat.baubles;
import baubles.api.BaubleType;
import io.tedsenft.gemstones.base.GemContainer;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Optional;

/**
 * Created by Ted on 2/23/2016.
 */
public class Amulet extends GemContainerBauble {

    public Amulet(){
        super(3);
        setUnlocalizedName("gems.containers.amulet");
        setRegistryName(Reference.MOD_ID, "amulet");
    }

    @Override
    @Optional.Method(modid = "Baubles")
    public BaubleType getBaubleType(ItemStack itemStack) {
        return BaubleType.AMULET;
    }
}
