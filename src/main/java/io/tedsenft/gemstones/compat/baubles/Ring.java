package io.tedsenft.gemstones.compat.baubles;

import baubles.api.BaubleType;
import io.tedsenft.gemstones.base.GemContainer;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Optional;

public class Ring extends GemContainerBauble {

  public Ring() throws IllegalArgumentException {
    super(1);
    setUnlocalizedName("gems.containers.ring");
    setRegistryName(Reference.MOD_ID, "ring");
  }

  @Override
  @Optional.Method(modid = "Baubles")
  public BaubleType getBaubleType(ItemStack itemStack) {
    return BaubleType.RING;
  }
}
