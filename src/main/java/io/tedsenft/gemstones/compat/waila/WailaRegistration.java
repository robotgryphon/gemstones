package io.tedsenft.gemstones.compat.waila;

import io.tedsenft.gemstones.base.GemstonesBlock;
import io.tedsenft.gemstones.blocks.crucible.BlockCrucible;
import io.tedsenft.gemstones.blocks.crucible.TileCrucible;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLInterModComms;

import java.util.List;

public class WailaRegistration {

  public static void register(){
    FMLInterModComms.sendMessage("Waila", "register", "io.tedsenft.gemstones.compat.waila.WailaRegistration.load");
  }

  public static void load(IWailaRegistrar reg){
    TileCrucible tc = new TileCrucible();
    reg.registerBodyProvider(tc, BlockCrucible.class);
    reg.registerNBTProvider(tc, BlockCrucible.class);
  }
}
