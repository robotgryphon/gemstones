package io.tedsenft.gemstones.reference;

/**
 * Created by Ted on 2/21/2016.
 */
public class Reference {

    public static final String MOD_ID = "gemstones";
    public static final String MOD_NAME = "Gemstones";
    public static final String MOD_VERSION = "1.0.0";

    public static final String PROXY_PACKAGE = "io.tedsenft.gemstones.proxy.";

    public static final String DEPENDENCIES = "required-after:Baubles";

}
