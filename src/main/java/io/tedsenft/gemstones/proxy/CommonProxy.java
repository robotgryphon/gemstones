package io.tedsenft.gemstones.proxy;

import io.tedsenft.gemstones.EventHandler;
import io.tedsenft.gemstones.base.GemstonesBlock;
import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.blocks.CrystalFormation;
import io.tedsenft.gemstones.blocks.GemstonesFluids;
import io.tedsenft.gemstones.blocks.crucible.BlockCrucible;
import io.tedsenft.gemstones.blocks.crucible.CrucibleRecipe;
import io.tedsenft.gemstones.blocks.crucible.TileCrucible;
import io.tedsenft.gemstones.blocks.pedestal.BlockPedestal;
import io.tedsenft.gemstones.blocks.pedestal.TilePedestal;
import io.tedsenft.gemstones.compat.baubles.Amulet;
import io.tedsenft.gemstones.compat.baubles.Ring;
import io.tedsenft.gemstones.items.CrucibleBucket;
import io.tedsenft.gemstones.items.CrystalShard;
import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.items.ItemHammer;
import io.tedsenft.gemstones.items.gem_containers.Staff;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import javax.vecmath.Vector3d;

/**
 * Created by Ted on 2/21/2016.
 */
public abstract class CommonProxy {

    public void preInit(){
        registerTileEntities();
        registerStuff();

        createCraftingRecipes();
        registerEventHandlers();
    }

    //region Pre-Initialization
    private void registerStuff(){
        GemstonesBlock.init(BlockPedestal.class);
        GemstonesBlock.init(CrystalFormation.class);

        GemstonesItem.init(ItemHammer.class);
        GemstonesItem.init(CrystalShard.class);

        // Gem Holders and gem itself
        if(Loader.isModLoaded("Baubles")) {
            GemstonesItem.init(Amulet.class);
            GemstonesItem.init(Ring.class);
        }

        GemstonesItem.init(Staff.class);

        GemstonesItem.init(Gemstone.class);

        // Crucible
        GemstonesBlock.init(BlockCrucible.class);
        GemstonesItem.init(CrucibleBucket.class);


        FluidRegistry.registerFluid(GemstonesFluids.meltedCrystal);
        FluidRegistry.addBucketForFluid(GemstonesFluids.meltedCrystal);
    }

    public void createCraftingRecipes(){
        GameRegistry.addRecipe(new ShapedOreRecipe( new ItemStack(GemstonesItem.instance(Staff.class), 1), "  w", " w ", "w  ", 'w', "plankWood"));
        GameRegistry.addRecipe(new ShapelessOreRecipe( new ItemStack(GemstonesItem.instance(CrystalShard.class), 1), GemstonesItem.instance(ItemHammer.class), "gem" ));
        GameRegistry.addRecipe(new ShapedOreRecipe( new ItemStack(GemstonesItem.instance(ItemHammer.class)), "iii", "isi", " s ", 'i', "ingotIron", 's', "stickWood"));

        GameRegistry.addRecipe(new ShapelessOreRecipe( new ItemStack(BlockPedestal.instance(BlockPedestal.class), 1), GemstonesItem.instance(ItemHammer.class), "plankWood"));
    }

    public void registerTileEntities() {
        GameRegistry.registerTileEntity(TilePedestal.class, Reference.MOD_ID + ":te_workbench");
        GameRegistry.registerTileEntity(TileCrucible.class, Reference.MOD_ID + ":te_crucible");
    }

    public void registerEventHandlers() {
        EventHandler.instance = new EventHandler();
        MinecraftForge.EVENT_BUS.register(EventHandler.instance);
    }

    public void registerCrucibleRecipes(){
        CrucibleRecipe meltedCrystal = new CrucibleRecipe(GemstonesFluids.meltedCrystal, new ItemStack[]{ new ItemStack(GemstonesItem.instance(CrystalShard.class), 16)});
        meltedCrystal.register();
    }

    public void registerPackets() { }
    //endregion

    public void playSound(ResourceLocation soundName, SoundCategory cat, BlockPos position, float volume, float pitch) { this.playSound(soundName, cat, new Vector3d(position.getX(), position.getY(), position.getZ()), volume, pitch); }
    public void playSound(ResourceLocation soundName, SoundCategory cat, Vector3d position, float volume, float pitch) { }
}
