package io.tedsenft.gemstones.proxy;

import io.tedsenft.gemstones.items.CrucibleBucket;
import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.items.CrystalShard;
import io.tedsenft.gemstones.blocks.pedestal.RenderPedestal;
import io.tedsenft.gemstones.blocks.pedestal.TilePedestal;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.fml.client.registry.ClientRegistry;

import javax.vecmath.Vector3d;

/**
 * Created by Ted on 2/21/2016.
 */
public class ClientProxy extends CommonProxy {


    public ClientProxy getClientProxy() {
        return this;
    }

    @Override
    public void preInit() {
        super.preInit();
        initRenderingAndTextures();
        registerKeybindings();
    }

    public void initRenderingAndTextures() {
        GemstonesItem.initRendering(CrystalShard.class);
        GemstonesItem.initRendering(CrucibleBucket.class);
        GemstonesItem.initRendering(Gemstone.class);

        ClientRegistry.bindTileEntitySpecialRenderer(TilePedestal.class, new RenderPedestal());

        B3DLoader.INSTANCE.addDomain(Reference.MOD_ID);
    }

    public void registerKeybindings() {

    }

    @Override
    public void playSound(ResourceLocation soundName, SoundCategory cat, Vector3d position, float volume, float pitch) {
        Minecraft.getMinecraft().theWorld.playSound(position.x, position.y, position.z, new SoundEvent(soundName), cat, volume, pitch, false);
    }

}
