package io.tedsenft.gemstones.blocks;

import io.tedsenft.gemstones.achievements.AchList;
import io.tedsenft.gemstones.base.GemstonesBlock;
import io.tedsenft.gemstones.items.CrystalShard;
import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CrystalFormation extends GemstonesBlock {

  public static PropertyDirection Facing = PropertyDirection.create("facing");

  public CrystalFormation(){
    super(Material.glass);
    setStepSound(SoundType.GLASS);
    setUnlocalizedName("crystal_formation");
    setRegistryName(Reference.MOD_ID, "crystal_formation");
    setHardness(0.3f);
    setHarvestLevel("pickaxe", 1);

    setDefaultState(this.getDefaultState().withProperty(Facing, EnumFacing.DOWN));
  }

  @Override
  public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
    return false;
  }

  @Override
  public boolean isOpaqueCube(IBlockState state) {
    return false;
  }

  @Override
  public boolean isVisuallyOpaque() {
    return false;
  }

  @Override
  public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
    Random rng = new Random();
    int addedByFortune = 0;
    if(fortune > 0) addedByFortune = rng.nextInt(2 * fortune);

    ArrayList<ItemStack> returned = new ArrayList<ItemStack>();
    returned.add(new ItemStack(GemstonesItem.instance(CrystalShard.class), 3 + addedByFortune));

    return returned;
  }


  @Override
  protected BlockStateContainer createBlockState() {
    return new BlockStateContainer(this, Facing);
  }

  @Override
  public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
    return state;
  }

  @Override
  public IBlockState getStateFromMeta(int meta) {
    return this.getDefaultState();
  }

  @Override
  public int getMetaFromState(IBlockState state) {
    return 0;
  }

  @Override
  public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player) {
    super.onBlockHarvested(worldIn, pos, state, player);

    player.addStat(AchList.gettingStarted, 1);
  }
}
