package io.tedsenft.gemstones.blocks.crucible;

import com.mojang.realmsclient.gui.ChatFormatting;
import com.sun.corba.se.impl.protocol.giopmsgheaders.MessageHandler;
import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.items.CrucibleBucket;
import io.tedsenft.gemstones.messages.CrucibleFuelUpdate;
import io.tedsenft.gemstones.utils.LogHelper;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class TileCrucible extends TileEntity implements net.minecraft.util.ITickable,IFluidHandler,IWailaDataProvider {

  // Bucket handler - handles melted fluid and reagent in bucket
  public BucketHandler bucket;

  // Fuel type- liquid. Probably lava.
  public FluidStack fuel;
  private final int MAX_FUEL = 1000;

  public TileCrucible(){
    this.bucket = new BucketHandler();
    this.fuel = null;
  }

  public TileCrucible(World w){
    this.bucket = new BucketHandler();
    this.fuel = null;
    this.setWorldObj(w);
  }

  public boolean hasBucket(){
    return bucket.hasBucket();
  }

  public boolean setBucket(ItemStack bucket){
    if(bucket.getItem() instanceof CrucibleBucket) {
      this.bucket.setStackInSlot(0, bucket);
      return true;
    }

    return false;
  }

  public ItemStack getBucket(){
    if(!this.hasBucket()) return null;
    ItemStack bucketRet = bucket.extractItem(0, 1, false);
    return bucketRet;
  }

  @Override
  public void readFromNBT(NBTTagCompound compound) {
    super.readFromNBT(compound);
    if(compound.hasKey("fuel")){
      fuel = FluidStack.loadFluidStackFromNBT(compound.getCompoundTag("fuel"));
    }
  }

  @Override
  public void writeToNBT(NBTTagCompound compound) {
    super.writeToNBT(compound);
    if(this.fuel != null){
      compound.setTag("fuel", fuel.writeToNBT(new NBTTagCompound()));
    }
  }

  /**
   * Fills fluid into internal tanks, distribution is left entirely to the IFluidHandler.
   *
   * @param from     Orientation the Fluid is pumped in from.
   * @param resource FluidStack representing the Fluid and maximum amount of fluid to be filled.
   * @param doFill   If false, fill will only be simulated.
   * @return Amount of resource that was (or would have been, if simulated) filled.
   */
  @Override
  public int fill(EnumFacing from, FluidStack resource, boolean doFill) {
    try {
      if (fuel != null && !fuel.isFluidEqual(resource)) return 0;

      if (fuel == null) {
        if (doFill){ this.fuel = resource; updateFuelStatus(); }
        return MAX_FUEL;
      }

      if (fuel.amount < MAX_FUEL) {
        int remaining = MAX_FUEL - fuel.amount;
        int canFill = Math.min(remaining, resource.amount);
        if (doFill){ fuel.amount += canFill; updateFuelStatus(); }
        return canFill;
      }
    }

    catch(Exception e){
      LogHelper.log(e);
    }

    return 0;
  }

  /**
   * Drains fluid out of internal tanks, distribution is left entirely to the IFluidHandler.
   *
   * @param from     Orientation the Fluid is drained to.
   * @param resource FluidStack representing the Fluid and maximum amount of fluid to be drained.
   * @param doDrain  If false, drain will only be simulated.
   * @return FluidStack representing the Fluid and amount that was (or would have been, if
   * simulated) drained.
   */
  @Override
  public FluidStack drain(EnumFacing from, FluidStack resource, boolean doDrain) {
    if(!resource.isFluidEqual(fuel)) return null;
    return drain(from, resource.amount, doDrain);
  }

  /**
   * Drains fluid out of internal tanks, distribution is left entirely to the IFluidHandler.
   * <p/>
   * This method is not Fluid-sensitive.
   *
   * @param from     Orientation the fluid is drained to.
   * @param maxDrain Maximum amount of fluid to drain.
   * @param doDrain  If false, drain will only be simulated.
   * @return FluidStack representing the Fluid and amount that was (or would have been, if
   * simulated) drained.
   */
  @Override
  public FluidStack drain(EnumFacing from, int maxDrain, boolean doDrain) {
    if(fuel == null) return null;

    FluidStack drained = fuel.copy();
    drained.amount = Math.min(drained.amount, maxDrain);
    if(doDrain){ fuel.amount -= drained.amount; if(fuel.amount == 0) fuel = null; updateFuelStatus(); }
    return drained;
  }

  private void updateFuelStatus(){
    if(this.worldObj == null) return;
    Gemstones.network.sendToAllAround(new CrucibleFuelUpdate(pos, fuel), new NetworkRegistry.TargetPoint(worldObj.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 10.0));
  }

  /**
   * Returns true if the given fluid can be inserted into the given direction.
   * <p/>
   * More formally, this should return true if fluid is able to enter from the given direction.
   *
   * @param from
   * @param fluid
   */
  @Override
  public boolean canFill(EnumFacing from, Fluid fluid) {
    return from != EnumFacing.DOWN && from != EnumFacing.UP;
  }

  /**
   * Returns true if the given fluid can be extracted from the given direction.
   * <p/>
   * More formally, this should return true if fluid is able to leave from the given direction.
   *
   * @param from
   * @param fluid
   */
  @Override
  public boolean canDrain(EnumFacing from, Fluid fluid) {
    return from != EnumFacing.DOWN && from != EnumFacing.UP;
  }

  /**
   * Returns an array of objects which represent the internal tanks. These objects cannot be used
   * to manipulate the internal tanks. See {@link FluidTankInfo}.
   *
   * @param from Orientation determining which tanks should be queried.
   * @return Info for the relevant internal tanks.
   */
  @Override
  public FluidTankInfo[] getTankInfo(EnumFacing from) {
    return new FluidTankInfo[]{ new FluidTankInfo(fuel, 1000) };
  }

  @Override
  public ItemStack getWailaStack(IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
    return null;
  }

  @Override
  public List<String> getWailaHead(ItemStack itemStack, List<String> list, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
    return list;
  }

  @Override
  public List<String> getWailaBody(ItemStack itemStack, List<String> list, IWailaDataAccessor data, IWailaConfigHandler config) {
    NBTTagCompound tags = data.getNBTData();
    if(tags.hasKey("fuel")){
      FluidStack fuel = FluidStack.loadFluidStackFromNBT(tags.getCompoundTag("fuel"));
      list.add("Fuel: " + ChatFormatting.LIGHT_PURPLE + fuel.getLocalizedName() + ChatFormatting.WHITE + " (" + ChatFormatting.GOLD + fuel.amount + ChatFormatting.WHITE + ")");
    }

    return list;
  }

  @Override
  public List<String> getWailaTail(ItemStack itemStack, List<String> list, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
    return list;
  }

  @Override
  public NBTTagCompound getNBTData(EntityPlayerMP entityPlayerMP, TileEntity tileEntity, NBTTagCompound tags, World world, BlockPos blockPos) {
    if(tileEntity instanceof TileCrucible){
      TileCrucible tc = (TileCrucible) tileEntity;
      if(tc.fuel != null){
        tags.setTag("fuel", tc.fuel.writeToNBT(new NBTTagCompound()));
      }
    }

    return tags;
  }

  @Override
  public void update() {
    if(this.fuel != null){
      this.fuel.amount --;
      if(this.fuel.amount == 0) this.fuel = null;
    }
  }

  public class BucketHandler implements IItemHandlerModifiable {

    private ItemStack bucket;
    private ItemStack melting;

    public boolean hasBucket(){
      return this.bucket != null;
    }

    @Override
    public void setStackInSlot(int slot, ItemStack stack) {
      switch(slot){
        case 0:
          bucket = stack;
          return;
        case 1:
          melting = stack;
          return;
        default:
          return;
      }
    }

    @Override
    public int getSlots() {
      return 2;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
      switch(slot){
        case 0:
          return bucket;
        case 1:
          return melting;
        default:
          return null;
      }
    }

    @Override
    public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
      ItemStack copy = stack.copy();
      switch(slot){
        case 0:
          if(!simulate && bucket == null){ bucket = stack; return null; }
          if(bucket == null) return null;
          return stack;
        case 1:
          // TODO: Check for better implementation or existing method
          if(melting != null) {

            if(ItemHandlerHelper.canItemStacksStack(melting, stack)){
              int ableToAdd = melting.getMaxStackSize() - melting.stackSize;
              if(copy.stackSize > ableToAdd){
                copy.stackSize -= ableToAdd;
                if(!simulate) melting.stackSize = melting.getMaxStackSize();
                return copy;
              } else {
                // <=, entire bit allowed in
                if(!simulate) melting.stackSize += copy.stackSize;
                return null;
              }
            } else {
              return stack;
            }
          }

          melting = stack;
          return null;
        default:
          return stack;
      }
    }

    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
      if(amount == 0) return null;
      switch(slot){
        case 0:
          ItemStack bCopy = bucket.copy();
          if(!simulate) bucket = null;
          return bCopy;

        case 1:
          if(melting == null || melting.stackSize == 0) return null;
          ItemStack mCopy = melting.copy();
          if(amount < mCopy.stackSize) {
            mCopy.stackSize -= amount;
            if(!simulate) melting.stackSize = mCopy.stackSize;
            return mCopy;
          }

          if(!simulate) melting = null;
          return mCopy;

        default:
          return null;
      }
    }
  }

  @Override
  public Packet getDescriptionPacket() {
    NBTTagCompound tag = new NBTTagCompound();
    writeToNBT(tag);
    return new SPacketUpdateTileEntity(this.pos, 0, tag);
  }

  @Override
  public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
    NBTTagCompound tag = pkt.getNbtCompound();
    readFromNBT(tag);
  }
}
