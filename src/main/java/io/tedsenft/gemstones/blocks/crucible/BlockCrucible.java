package io.tedsenft.gemstones.blocks.crucible;

import io.tedsenft.gemstones.base.GemstonesBlock;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;

public class BlockCrucible extends GemstonesBlock
{

  public static final PropertyBool HasBucket = PropertyBool.create("has_bucket");

  public BlockCrucible(){
    super(Material.iron);
    setHardness(1f);

    setRegistryName(Reference.MOD_ID, "crucible");
    setUnlocalizedName("crucible");
    setDefaultState(getDefaultState().withProperty(HasBucket, false));
  }

  @Override
  public boolean isOpaqueCube(IBlockState state) {
    return false;
  }

  @Override
  public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
    return false;
  }

  @Override
  protected BlockStateContainer createBlockState() {
    return new ExtendedBlockState(this, new IProperty[]{ HasBucket }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.INSTANCE});
  }

  @Override
  public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos) {
    B3DLoader.B3DState newState = new B3DLoader.B3DState(null, 0);
    return ((IExtendedBlockState) state).withProperty(B3DLoader.B3DFrameProperty.INSTANCE, newState);
  }

  @Override
  public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
    TileCrucible tc = (TileCrucible) worldIn.getTileEntity(pos);
    if(tc == null) return state;

    return state.withProperty(HasBucket, tc.bucket.hasBucket());
  }

  @Override
  public IBlockState getStateFromMeta(int meta) {
    return getDefaultState();
  }

  @Override
  public int getMetaFromState(IBlockState state) {
    return 0;
  }

  @Override
  public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
    try {
      TileCrucible tc = (TileCrucible) worldIn.getTileEntity(pos);
      if (tc.fuel != null) playerIn.addChatComponentMessage(new TextComponentString(TextFormatting.AQUA + (worldIn.isRemote ? "Client:: " : "Server:: " ) +
              TextFormatting.WHITE + tc.fuel.getLocalizedName() + ": " + tc.fuel.amount));
      else
        if(worldIn.isRemote) playerIn.addChatComponentMessage(new TextComponentString("No fuel"));
    }

    catch(Exception e){

    }

    return true;
  }

  @Override
  public boolean hasTileEntity(IBlockState state) {
    return true;
  }

  @Override
  public TileEntity createTileEntity(World world, IBlockState state) {
    return new TileCrucible(world);
  }
}
