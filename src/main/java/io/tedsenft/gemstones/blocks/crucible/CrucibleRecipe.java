package io.tedsenft.gemstones.blocks.crucible;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;

public class CrucibleRecipe {

  private Fluid output;
  private ItemStack[] inputs;

  public CrucibleRecipe(Fluid output, ItemStack[] inputs){
    this.inputs = inputs;
    this.output = output;
  }

  public CrucibleRecipe register(){

    return this;
  }

}
