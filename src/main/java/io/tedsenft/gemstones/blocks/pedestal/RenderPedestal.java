package io.tedsenft.gemstones.blocks.pedestal;

import io.tedsenft.gemstones.utils.MathUtils;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

import javax.vecmath.Vector2f;

public class RenderPedestal extends TileEntitySpecialRenderer<TilePedestal> {

  @Override
  public void renderTileEntityAt(TilePedestal te, double x, double y, double z, float partialTicks, int destroyStage) {
    // Render gem
    // Render billboard
    // Profit

    GlStateManager.pushMatrix();

    GlStateManager.translate(x, y, z);

    VertexBuffer wr = Tessellator.getInstance().getBuffer();

    GlStateManager.disableLighting();
    GlStateManager.disableBlend();

    // if(te.gem != null){

    wr.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);

    double angle = te.getNextAngularPosition(0.5f);
    Vector2f posOffset = MathUtils.getPointOnCircle(new Vector2f(0,0), 4f, (float) angle);

    GL11.glLineWidth(3.0f);
    wr.pos(0.5f, 1.2f, 0.5f).color(1,0,0,1).endVertex();
    wr.pos(0.5f + posOffset.x, 1.4f, 0.5f + posOffset.y).color(1,1,0,1).endVertex();



    // }

    Tessellator.getInstance().draw();
    GlStateManager.popMatrix();
  }
}
