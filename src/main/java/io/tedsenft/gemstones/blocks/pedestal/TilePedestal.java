package io.tedsenft.gemstones.blocks.pedestal;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.IItemHandlerModifiable;

public class TilePedestal extends TileEntity {

  public GemHandler gemHandler;
  public double lastAngle = 0;
  public long lastTime = 0;

  public TilePedestal(){
    this.gemHandler = new GemHandler();
  }

  @Override
  public void readFromNBT(NBTTagCompound compound) {
    super.readFromNBT(compound);

    if(compound.hasKey("gem")) gemHandler.setStackInSlot(0, ItemStack.loadItemStackFromNBT(compound.getCompoundTag("gem")));
  }

  @Override
  public void writeToNBT(NBTTagCompound compound) {
    super.writeToNBT(compound);

    if(gemHandler.getStackInSlot(0) != null){
      ItemStack gem = gemHandler.getStackInSlot(0);
      NBTTagCompound gemNBT = gem.writeToNBT(new NBTTagCompound());
      compound.setTag("gem", gemNBT);
    }
  }

  public double getNextAngularPosition(double revsPerSecond) {
    // we calculate the next position as the angular speed multiplied by the elapsed time since the last position.
    // Elapsed time is calculated using the system clock, which means the animations continue to
    //  run while the game is paused.
    // Alternatively, the elapsed time can be calculated as
    //  time_in_seconds = (number_of_ticks_elapsed + partialTick) / 20.0;
    //  where your tileEntity's update() method increments number_of_ticks_elapsed, and partialTick is passed by vanilla
    //   to your TESR renderTileEntityAt() method.
    long timeNow = System.nanoTime();
    if (lastTime == 0) {   // automatically initialise to 0 if not set yet
      lastTime = timeNow;
      lastAngle = 0.0f;
    }
    final double DEGREES_PER_REV = 360.0;
    final double NANOSECONDS_PER_SECOND = 1e9;
    double nextAngularPosition = lastAngle + (timeNow - lastTime) * revsPerSecond * DEGREES_PER_REV / NANOSECONDS_PER_SECOND;
    nextAngularPosition = nextAngularPosition % DEGREES_PER_REV;
    lastAngle = nextAngularPosition;
    lastTime = timeNow;
    return nextAngularPosition;
  }

  public class GemHandler implements IItemHandlerModifiable {

    private ItemStack gem;

    /**
     * Overrides the stack in the given slot. This method is used by the
     * standard Forge helper methods and classes. It is not intended for
     * general use by other mods, and the handler may throw an error if it
     * is called unexpectedly.
     *
     * @param slot  Slot to modify
     * @param stack ItemStack to set slot to (may be null)
     * @throws RuntimeException if the handler is called in a way that the handler
     *                          was not expecting.
     **/
    @Override
    public void setStackInSlot(int slot, ItemStack stack) {
      gem = stack;
    }

    /**
     * Returns the number of slots available
     *
     * @return The number of slots available
     **/
    @Override
    public int getSlots() {
      return 1;
    }

    /**
     * Returns the ItemStack in a given slot.
     * <p>
     * The result's stack size may be greater than the itemstacks max size.
     * <p>
     * If the result is null, then the slot is empty.
     * If the result is not null but the stack size is zero, then it represents
     * an empty slot that will only accept* a specific itemstack.
     * <p>
     * <p>
     * IMPORTANT: This ItemStack MUST NOT be modified. This method is not for
     * altering an inventories contents. Any implementers who are able to detect
     * modification through this method should throw an exception.
     * <p>
     * SERIOUSLY: DO NOT MODIFY THE RETURNED ITEMSTACK
     *
     * @param slot Slot to query
     * @return ItemStack in given slot. May be null.
     **/
    @Override
    public ItemStack getStackInSlot(int slot) {
      return slot == 0 ? gem : null;
    }

    /**
     * Inserts an ItemStack into the given slot and return the remainder.
     * The ItemStack should not be modified in this function!
     * Note: This behaviour is subtly different from IFluidHandlers.fill()
     *
     * @param slot     Slot to insert into.
     * @param stack    ItemStack to insert.
     * @param simulate If true, the insertion is only simulated
     * @return The remaining ItemStack that was not inserted (if the entire stack is accepted, then return null).
     * May be the same as the input ItemStack if unchanged, otherwise a new ItemStack.
     **/
    @Override
    public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
      if(gem != null) return stack;

      if(!simulate) gem = stack;
      return null;

    }

    /**
     * Extracts an ItemStack from the given slot. The returned value must be null
     * if nothing is extracted, otherwise it's stack size must not be greater than amount or the
     * itemstacks getMaxStackSize().
     *
     * @param slot     Slot to extract from.
     * @param amount   Amount to extract (may be greater than the current stacks max limit)
     * @param simulate If true, the extraction is only simulated
     * @return ItemStack extracted from the slot, must be null, if nothing can be extracted
     **/
    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
      if(slot != 0 || gem == null) return null;
      ItemStack temp = gem.copy();
      if(!simulate) gem = null;
      return temp;
    }
  }
}
