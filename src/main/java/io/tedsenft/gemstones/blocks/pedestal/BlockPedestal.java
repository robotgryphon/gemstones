package io.tedsenft.gemstones.blocks.pedestal;

import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.base.GemstonesBlock;
import io.tedsenft.gemstones.reference.Guis;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;

/**
 * Created by Ted on 2/27/2016.
 */
public class BlockPedestal extends GemstonesBlock {

    public static final String unlocalName = "gem_pedestal";

    public ExtendedBlockState state = new ExtendedBlockState(this, new IProperty[]{ }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.INSTANCE});

    public BlockPedestal() {
        super(Material.wood);
        setUnlocalizedName(unlocalName);
        setRegistryName(Reference.MOD_ID, "gem_pedestal");
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
        return false;
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new ExtendedBlockState(this, new IProperty[]{ }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.INSTANCE});
    }

    @Override
    public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos) {
        B3DLoader.B3DState newState = new B3DLoader.B3DState(null, 0);
        return ((IExtendedBlockState) state).withProperty(B3DLoader.B3DFrameProperty.INSTANCE, newState);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
        playerIn.openGui(Gemstones.instance, Guis.PEDESTAL, worldIn, pos.getX(), pos.getY(), pos.getZ());
        return true;
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return state.getBlock() instanceof BlockPedestal;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TilePedestal();
    }
}
