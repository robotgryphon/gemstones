package io.tedsenft.gemstones.blocks;

import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;

public class GemstonesFluids {

  public static Fluid meltedCrystal = new Fluid("meltedCrystal", new ResourceLocation(Reference.MOD_ID, "fluids/crystal_still"), new ResourceLocation(Reference.MOD_ID, "fluids/crystal_flowing"));

}
