package io.tedsenft.gemstones.utils;

import javax.vecmath.Vector2f;

/**
 * Created by Ted on 2/23/2016.
 */
public class MathUtils {

    public static Vector2f getPointOnCircle(Vector2f center, float radius, float angleInDegrees){
        Vector2f result = new Vector2f(0,0);

        double radAngle = Math.toRadians(angleInDegrees);

        result.x = Math.round( center.x + (radius * Math.cos( radAngle )) );
        result.y = Math.round( center.y + (radius * Math.sin( radAngle )) );

        return result;
    }
}
