package io.tedsenft.gemstones.utils;

import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by Ted on 2/23/2016.
 */
public class ModelHelper {

    public static void registerModel(Item i, String variant){
        ModelLoader.setCustomModelResourceLocation(i, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + i.getUnlocalizedName().substring(5), variant));
    }
}
