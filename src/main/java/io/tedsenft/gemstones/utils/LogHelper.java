package io.tedsenft.gemstones.utils;

import org.apache.logging.log4j.Logger;

/**
 * Created by Ted on 2/24/2016.
 */
public class LogHelper {

    public static Logger instance;

    public static void log(Object o){
        instance.info(String.valueOf(o));
    }
}
