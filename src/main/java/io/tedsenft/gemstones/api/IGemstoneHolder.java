package io.tedsenft.gemstones.api;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

/**
 * Created by Ted on 2/28/2016.
 */
public interface IGemstoneHolder {

    int getNumberSlots();

    ItemStack[] getGems(ItemStack container);

    boolean handleAttacking(ItemStack weapon, EntityLivingBase attacked);
    void update(ItemStack item, EntityLivingBase holder);
    void onEquip(ItemStack item, EntityLivingBase equipper);
    void onUnequip(ItemStack item, EntityLivingBase unequipper);
    void handleDamage(ItemStack container, LivingAttackEvent event);
}
