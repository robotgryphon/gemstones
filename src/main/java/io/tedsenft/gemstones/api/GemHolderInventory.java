package io.tedsenft.gemstones.api;

import io.tedsenft.gemstones.items.Gemstone;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumHand;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.items.IItemHandlerModifiable;

/**
 * Created by Ted on 2/25/2016.
 */
public class GemHolderInventory {

    private EntityPlayer player;
    public IItemHandlerModifiable gemHolder;
    public IItemHandlerModifiable gems;

    public GemHolderInventory(EntityPlayer player, ItemStack gh){
        this.player = player;
        this.gemHolder = new GemHolderWrapper(gh);
        this.gems = new GemInventory(this);
    }

    public int getNumAvailableSlots(){
        return gemHolder.getStackInSlot(0) != null ?
                ((IGemstoneHolder) gemHolder.getStackInSlot(0).getItem()).getNumberSlots() : 0;
    }

    public class GemHolderWrapper implements IItemHandlerModifiable {

        private ItemStack gemHolder;
        public GemHolderWrapper(ItemStack holder){
            this.gemHolder = holder;
        }

        /**
         * Overrides the stack in the given slot. This method is used by the
         * standard Forge helper methods and classes. It is not intended for
         * general use by other mods, and the handler may throw an error if it
         * is called unexpectedly.
         *
         * @param slot  Slot to modify
         * @param stack ItemStack to set slot to (may be null)
         * @throws RuntimeException if the handler is called in a way that the handler
         *                          was not expecting.
         **/
        @Override
        public void setStackInSlot(int slot, ItemStack stack) {
            this.gemHolder = stack;
        }

        /**
         * Returns the number of slots available
         *
         * @return The number of slots available
         **/
        @Override
        public int getSlots() {
            return 1;
        }

        /**
         * Returns the ItemStack in a given slot.
         * <p/>
         * The result's stack size may be greater than the itemstacks max size.
         * <p/>
         * If the result is null, then the slot is empty.
         * If the result is not null but the stack size is zero, then it represents
         * an empty slot that will only accept* a specific itemstack.
         * <p/>
         * <p/>
         * IMPORTANT: This ItemStack MUST NOT be modified. This method is not for
         * altering an inventories contents. Any implementers who are able to detect
         * modification through this method should throw an exception.
         * <p/>
         * SERIOUSLY: DO NOT MODIFY THE RETURNED ITEMSTACK
         *
         * @param slot Slot to query
         * @return ItemStack in given slot. May be null.
         **/
        @Override
        public ItemStack getStackInSlot(int slot) {
            return gemHolder;
        }

        /**
         * Inserts an ItemStack into the given slot and return the remainder.
         * The ItemStack should not be modified in this function!
         * Note: This behaviour is subtly different from IFluidHandlers.fill()
         *
         * @param slot     Slot to insert into.
         * @param stack    ItemStack to insert.
         * @param simulate If true, the insertion is only simulated
         * @return The remaining ItemStack that was not inserted (if the entire stack is accepted, then return null).
         * May be the same as the input ItemStack if unchanged, otherwise a new ItemStack.
         **/
        @Override
        public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
            if(gemHolder != null) return stack;

            if(gemHolder == null && stack.getItem() instanceof IGemstoneHolder){
                if(!simulate) gemHolder = stack;
                return null;
            }

            return stack;
        }

        /**
         * Extracts an ItemStack from the given slot. The returned value must be null
         * if nothing is extracted, otherwise it's stack size must not be greater than amount or the
         * itemstacks getMaxStackSize().
         *
         * @param slot     Slot to extract from.
         * @param amount   Amount to extract (may be greater than the current stacks max limit)
         * @param simulate If true, the extraction is only simulated
         * @return ItemStack extracted from the slot, must be null, if nothing can be extracted
         **/
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if(slot == 0 && amount > 0 && gemHolder != null){
                ItemStack holder = gemHolder;
                if(!simulate) gemHolder = null;
                return holder;
            }

            return null;
        }
    }

    public class GemInventory implements IItemHandlerModifiable {

        private GemHolderInventory master;
        private ItemStack[] gems;

        public GemInventory(GemHolderInventory master){
            this.master = master;
            this.gems = new ItemStack[master.getNumAvailableSlots()];

            loadGems();
        }

        /**
         * Returns the number of slots available
         *
         * @return The number of slots available
         **/
        @Override
        public int getSlots() {
            return master.getNumAvailableSlots();
        }

        public void loadGems(){
            if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) return;
            ItemStack container = master.gemHolder.getStackInSlot(0);
            if(container == null) return; // Wat

            // Also shouldn't happen, but hey
            if(container.getTagCompound() == null) container.setTagCompound(new NBTTagCompound());

            NBTTagCompound tags = container.getTagCompound();
            for(int slot = 0; slot < getSlots(); slot++){
                if(tags.hasKey("gem_" + slot)) gems[slot] = ItemStack.loadItemStackFromNBT(tags.getCompoundTag("gem_" + slot));
            }
        }

        /**
         * Returns the ItemStack in a given slot.
         * <p/>
         * The result's stack size may be greater than the itemstacks max size.
         * <p/>
         * If the result is null, then the slot is empty.
         * If the result is not null but the stack size is zero, then it represents
         * an empty slot that will only accept* a specific itemstack.
         * <p/>
         * <p/>
         * IMPORTANT: This ItemStack MUST NOT be modified. This method is not for
         * altering an inventories contents. Any implementers who are able to detect
         * modification through this method should throw an exception.
         * <p/>
         * SERIOUSLY: DO NOT MODIFY THE RETURNED ITEMSTACK
         *
         * @param slot Slot to query
         * @return ItemStack in given slot. May be null.
         **/
        @Override
        public ItemStack getStackInSlot(int slot) {
            if(!isValidSlot(slot)) return null;
            return gems[slot];
        }

        private boolean isValidSlot(int slot){
            return slot > -1 && slot < getSlots();
        }

        /**
         * Inserts an ItemStack into the given slot and return the remainder.
         * The ItemStack should not be modified in this function!
         * Note: This behaviour is subtly different from IFluidHandlers.fill()
         *
         * @param slot     Slot to insert into.
         * @param stack    ItemStack to insert.
         * @param simulate If true, the insertion is only simulated
         * @return The remaining ItemStack that was not inserted (if the entire stack is accepted, then return null).
         * May be the same as the input ItemStack if unchanged, otherwise a new ItemStack.
         **/
        @Override
        public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
            if(stack == null || stack.stackSize == 0) return null;

            if(stack.getItem() instanceof Gemstone){
                if(isValidSlot(slot) && gems[slot] == null){
                    if(!simulate){
                        gems[slot] = stack.copy();
                        onSlotChanged(slot);
                    }

                    return null;
                }
            }

            return stack;
        }

        /**
         * Extracts an ItemStack from the given slot. The returned value must be null
         * if nothing is extracted, otherwise it's stack size must not be greater than amount or the
         * itemstacks getMaxStackSize().
         *
         * @param slot     Slot to extract from.
         * @param amount   Amount to extract (may be greater than the current stacks max limit)
         * @param simulate If true, the extraction is only simulated
         * @return ItemStack extracted from the slot, must be null, if nothing can be extracted
         **/
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if(!isValidSlot(slot)) return null;
            ItemStack stack = this.gems[slot].copy();
            if(!simulate){
                this.gems[slot] = null;
                onSlotChanged(slot);
            }

            return stack;
        }

        /**
         * Overrides the stack in the given slot. This method is used by the
         * standard Forge helper methods and classes. It is not intended for
         * general use by other mods, and the handler may throw an error if it
         * is called unexpectedly.
         *
         * @param slot  Slot to modify
         * @param stack ItemStack to set slot to (may be null)
         * @throws RuntimeException if the handler is called in a way that the handler
         *                          was not expecting.
         **/
        @Override
        public void setStackInSlot(int slot, ItemStack stack) {
            if(isValidSlot(slot)) {
                this.gems[slot] = stack;
                onSlotChanged(slot);
            }
        }

        public void onSlotChanged(int slot){
            if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) return;
            if(!isValidSlot(slot)) return;

            ItemStack container = master.gemHolder.getStackInSlot(0);
            if(container == null) return; // Wat

            // Also shouldn't happen, but hey
            if(container.getTagCompound() == null) container.setTagCompound(new NBTTagCompound());

            NBTTagCompound tags = container.getTagCompound();
            if(tags.hasKey("gem_" + slot)) tags.removeTag("gem_" + slot);

            if(gems[slot] != null) {
                NBTTagCompound gemData = gems[slot].writeToNBT(new NBTTagCompound());
                tags.setTag("gem_" + slot, gemData);
            }

            // sync nbt to client
            master.player.getHeldItem(EnumHand.MAIN_HAND).setTagCompound(tags);
        }
    }
}
