package io.tedsenft.gemstones.api;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

/**
 * Created by Ted on 2/25/2016.
 */
public interface IGemstoneAbility {

    /**
     * Used to add additional data to the nbt tags in a gem.
     * Do not return null here, additional information is added to this before saving/loading!
     * @param stack
     * @return
     */
    NBTTagCompound getNBT(ItemStack stack);

    /**
     * Required for getting an instance of the ability from registration.
     * Also helps if the registry needs to send any queries back to the mod.
     *
     * @return
     */
    String getModID();

    /**
    * This adds "gemstones.abilities." before whatever this returns.
    * It's used to look up a display name for a gemstone ability.
    *
    * @return
    */
    String getUnlocalizedName();

    /**
     * Used to check if a gem should apply it's effects.
     *
     * @param stack The current itemstack.
     * @return
     */
    boolean isActive(ItemStack stack);

    /**
     * Adds effects to a entity.
     * Called as part of a loop when the container item is equipped.
     *
     * @param stack The gemstone.
     * @param entity The entity to affect.
     */
    void addEffects(ItemStack stack, EntityLivingBase entity);

    /**
     * Removes effects from a entity.
     * Called as part of a loop when the container item is taken off.
     *
     * @param stack The gemstone.
     * @param entity The entity to affect.
     */
    void remEffects(ItemStack stack, EntityLivingBase entity);

    /**
     * Handles a given damage source for a entity.
     * Used for negating fire/void/falling/whatever damage.
     *
     * @param source
     * @param entity
     * @param stack
     */
    void handleDamage(LivingAttackEvent source, EntityLivingBase entity, ItemStack stack);

    boolean isActivatedBy(GemActivationType type);
}
