package io.tedsenft.gemstones.api;

public enum GemActivationType {
  /**
   * Ability is activated when its holder is put on, and ability is deactivated when holder unequipped
   */
  EQUIP,

  /**
   * Ability activated every tick the wearer has the gem equipped
   */
  TICK,

  /**
   * Ability is activated when the wearer has the gem equipped and takes damage from something.
   */
  ENTITY_DAMAGED,

  /**
   * Ability activated when the wearer goes to attack something.
   */
  ATTACKING;
}
