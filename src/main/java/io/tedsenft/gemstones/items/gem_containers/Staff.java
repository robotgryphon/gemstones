package io.tedsenft.gemstones.items.gem_containers;

import io.tedsenft.gemstones.base.GemContainer;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class Staff extends GemContainer {

  public Staff() {
    super(1);

    setUnlocalizedName("gems.containers.staff");
    setMaxStackSize(1);
    setRegistryName(Reference.MOD_ID, "staff");
  }

  @Override
  public boolean onEntitySwing(EntityLivingBase entityLiving, ItemStack stack) {
    return handleAttacking(stack, entityLiving);
  }

  @Override
  public boolean handleAttacking(ItemStack weapon, EntityLivingBase attacked) {
    // Activate any attacking abilities
    return false;
  }
}
