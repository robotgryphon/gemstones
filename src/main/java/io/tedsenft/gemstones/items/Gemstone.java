package io.tedsenft.gemstones.items;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.creativetabs.TabGems;
import io.tedsenft.gemstones.abilities.AbilityRegistry;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ted on 2/21/2016.
 */
public class Gemstone extends GemstonesItem {

    public Gemstone(){
        setCreativeTab(TabGems.instance);
        setNoRepair();
        setMaxStackSize(1);
        setRegistryName(Reference.MOD_ID, "gemstone");
        setUnlocalizedName("gemstone");
    }

    public boolean addAbility(ItemStack stack, IGemstoneAbility ability){
        NBTTagCompound abilityNBT = ability.getNBT(stack);
        if(abilityNBT == null) abilityNBT = new NBTTagCompound();

        abilityNBT.setString("map", ability.getModID() + ":" + ability.getUnlocalizedName());

        if(!stack.hasTagCompound()) stack.setTagCompound(new NBTTagCompound());
        NBTTagCompound gemNBT = stack.getTagCompound();

        if(!gemNBT.hasKey("abilities")) gemNBT.setTag("abilities", new NBTTagList());
        NBTTagList abilities = gemNBT.getTagList("abilities", Constants.NBT.TAG_COMPOUND);

        if(abilities.hasNoTags()) {
            abilities.appendTag(abilityNBT);
            return true;
        }

        for(int tag = 0; tag < abilities.tagCount(); ++tag){
            if(abilities.getCompoundTagAt(tag).getString("map").equals(ability.getModID() + ":" + ability.getUnlocalizedName()))
                return false;
        }

        abilities.appendTag(abilityNBT);
        return true;
    }

    public boolean removeAbility(ItemStack stack, IGemstoneAbility ability){
        if(!stack.hasTagCompound()) stack.setTagCompound(new NBTTagCompound());
        NBTTagCompound nbt = stack.getTagCompound();
        if(!nbt.hasKey("abilities")) nbt.setTag("abilities", new NBTTagList());

        NBTTagList abilities = nbt.getTagList("abilities", Constants.NBT.TAG_COMPOUND);
        if(abilities.hasNoTags()) return false;

        for(int i = 0; i < abilities.tagCount(); ++i){
            if(abilities.getCompoundTagAt(i).getString("map").equals(ability.getModID() + ":" + ability.getUnlocalizedName())){
                abilities.removeTag(i);
                stack.getTagCompound().setTag("abilities", abilities);
                return true;
            }
        }

        return false;
    }

    protected void createStackTag(ItemStack stack){
        if(!stack.hasTagCompound()) stack.setTagCompound(new NBTTagCompound());
        NBTTagCompound tags = new NBTTagCompound();
        if(!tags.hasKey("abilities")) tags.setTag("abilities", new NBTTagList());
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
        createStackTag(stack);
    }

    public IGemstoneAbility[] getAbilities(ItemStack stack){
        List<IGemstoneAbility> abs = new ArrayList<IGemstoneAbility>();
        if(!stack.hasTagCompound() || !stack.getTagCompound().hasKey("abilities")) createStackTag(stack);

        NBTTagList abList = stack.getTagCompound().getTagList("abilities", Constants.NBT.TAG_COMPOUND);
        for(int abid = 0; abid < abList.tagCount(); ++abid){
            IGemstoneAbility ab = AbilityRegistry.getAbilityByMap(abList.getCompoundTagAt(abid).getString("map"));
            abs.add(ab);
        }

        IGemstoneAbility[] list = new IGemstoneAbility[abList.tagCount()];
        return abs.toArray(list);
    }

    public boolean hasAnyActiveAbilities(ItemStack stack){
        if(!stack.hasTagCompound() || !stack.getTagCompound().hasKey("abilities")) return false;
        return stack.getTagCompound().getTagList("abilities", Constants.NBT.TAG_COMPOUND).tagCount() > 0;
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return hasAnyActiveAbilities(stack);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        if(!GuiScreen.isShiftKeyDown()) return;

        IGemstoneAbility[] abilities = getAbilities(stack);
        if(abilities.length == 0)
            tooltip.add(ChatFormatting.GRAY + I18n.translateToLocal("gemstones.messages.no_abilities"));
        else
            for(IGemstoneAbility ab : abilities) tooltip.add(ChatFormatting.GRAY + I18n.translateToLocal("gemstones.abilities." + ab.getUnlocalizedName()));
    }
}
