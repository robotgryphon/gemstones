package io.tedsenft.gemstones.items;

import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextFormatting;

import java.util.List;

public class CrucibleBucket extends GemstonesItem {

  public CrucibleBucket(){
    setMaxStackSize(1);
    setNoRepair();

    setUnlocalizedName("crucible_bucket");
    setRegistryName(Reference.MOD_ID, "crucible_bucket");
  }

  @Override
  public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
    if(!stack.hasTagCompound()) stack.setTagCompound(new NBTTagCompound());
    NBTTagCompound tags = stack.getTagCompound();

    tooltip.add("Melting: " + (tags.hasKey("reagent") ? ItemStack.loadItemStackFromNBT(tags.getCompoundTag("reagent")).getDisplayName() : "nothing"));

    // Add fluid information
    tooltip.add("Fluid: " + (tags.hasKey("fluid") ? tags.getString("fluid") : "none"));
    if (tags.hasKey("fluidAmount")) tooltip.add(TextFormatting.GOLD + String.valueOf(tags.getInteger("fluidAmount")) + "mb");

  }
}
