package io.tedsenft.gemstones.items;

import io.tedsenft.gemstones.base.GemstonesItem;
import io.tedsenft.gemstones.creativetabs.TabGems;
import io.tedsenft.gemstones.reference.Reference;

public class CrystalShard extends GemstonesItem {

  public CrystalShard(){
    setCreativeTab(TabGems.instance);
    setUnlocalizedName("crystal_shard");
    setRegistryName(Reference.MOD_ID, "crystal_shard");
    setMaxStackSize(16);
  }
}
