package io.tedsenft.gemstones.items;

import io.tedsenft.gemstones.achievements.AchList;
import io.tedsenft.gemstones.creativetabs.TabGemstones;
import io.tedsenft.gemstones.blocks.pedestal.BlockPedestal;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemHammer extends Item {

  public ItemHammer(){
    setCreativeTab(TabGemstones.instance);
    setMaxStackSize(1);
    setUnlocalizedName("hammer");
    setRegistryName(Reference.MOD_ID, "hammer");
  }

  @Override
  public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
    playerIn.addStat(AchList.gettingStarted, 1);
  }

  @Override
  public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
    if(worldIn.isRemote) return EnumActionResult.FAIL;

    IBlockState stateInPos = worldIn.getBlockState(pos);
    if(stateInPos.getBlock() instanceof BlockPlanks){
      IBlockState state = (IBlockState) new BlockStateContainer(GameRegistry.findBlock(Reference.MOD_ID, BlockPedestal.unlocalName));
      worldIn.setBlockState(pos, state);
      return EnumActionResult.SUCCESS;
    }

    return EnumActionResult.FAIL;
  }

  @Override
  public boolean hasContainerItem(ItemStack stack) {
    return true;
  }

  @Override
  public ItemStack getContainerItem(ItemStack itemStack) {
    return itemStack;
  }
}
