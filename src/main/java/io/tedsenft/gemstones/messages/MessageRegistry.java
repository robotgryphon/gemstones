package io.tedsenft.gemstones.messages;

import io.tedsenft.gemstones.Gemstones;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Created by Ted on 2/24/2016.
 */
public class MessageRegistry {

    public static void registerMessages(){
        Gemstones.network.registerMessage(CrucibleFuelUpdate.Handler.class, CrucibleFuelUpdate.class, 1, Side.CLIENT);
    }
}
