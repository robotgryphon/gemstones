package io.tedsenft.gemstones.messages;

import io.netty.buffer.ByteBuf;
import io.tedsenft.gemstones.blocks.crucible.TileCrucible;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class CrucibleFuelUpdate implements IMessage {

  private BlockPos pos;
  private FluidStack fluid;

  public CrucibleFuelUpdate(){

  }

  public CrucibleFuelUpdate(BlockPos pos, FluidStack stack){
    this.pos = pos;
    this.fluid = stack;
  }

  /**
   * Convert from the supplied buffer into your specific message type
   *
   * @param buf
   */
  @Override
  public void fromBytes(ByteBuf buf) {
    pos = BlockPos.fromLong(buf.readLong());
    NBTTagCompound fluidStack = ByteBufUtils.readTag(buf);
    if(!fluidStack.hasNoTags()) fluid = FluidStack.loadFluidStackFromNBT(fluidStack);
  }

  /**
   * Deconstruct your message into the supplied byte buffer
   *
   * @param buf
   */
  @Override
  public void toBytes(ByteBuf buf) {
    buf.writeLong(pos.toLong());
    if(fluid != null) ByteBufUtils.writeTag(buf, fluid.writeToNBT(new NBTTagCompound()));
    else ByteBufUtils.writeTag(buf, new NBTTagCompound());
  }

  public static class Handler implements IMessageHandler<CrucibleFuelUpdate, IMessage> {

    /**
     * Called when a message is received of the appropriate type. You can optionally return a reply message, or null if no reply
     * is needed.
     *
     * @param message The message
     * @param ctx
     * @return an optional return message
     */
    @Override
    public IMessage onMessage(CrucibleFuelUpdate message, MessageContext ctx) {
      if(ctx.side != Side.CLIENT) return null;

      World w = Minecraft.getMinecraft().theWorld;
      TileCrucible tc = (TileCrucible) w.getTileEntity(message.pos);
      tc.fuel = message.fluid;

      return null;
    }
  }
}
