package io.tedsenft.gemstones.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;

import javax.vecmath.Vector2f;
import java.util.ArrayList;

public class GuiUtils {

  public static Slot[] createStandardInventory(EntityPlayer owner, Vector2f position) {
    ArrayList<Slot> slots = new ArrayList<Slot>();

    // Inventory
    Vector2f slotPos = new Vector2f(1, 2);
    for (int slot = 9; slot < owner.inventory.mainInventory.length; slot++) {
      slots.add(new Slot(owner.inventory, slot,
              ((int) slotPos.x - 1) * 18 + (int) position.x,
              8 + (int) position.y + (((int) slotPos.y - 1) * 18)));

      slotPos.x++;
      if (slotPos.x == 10) {
        slotPos.x = 1;
        slotPos.y++;
      }
    }

    // Hotbar
    position.y += 28 + (18 * 3);
    slotPos = new Vector2f(1, 1);
    for(int slot = 0; slot < 9; slot++) {
      slots.add(new Slot(owner.inventory, slot,
              ((int) slotPos.x - 1) * 18 + (int) position.x,
              8 + (((int) slotPos.y - 1) * 18) + (int) position.y));

      slotPos.x++;
    }

    Slot[] slotsRet = new Slot[slots.size()];
    return slots.toArray(slotsRet);
  }
}
