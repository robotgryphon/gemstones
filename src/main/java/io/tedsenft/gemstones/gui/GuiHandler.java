package io.tedsenft.gemstones.gui;

import io.tedsenft.gemstones.blocks.crucible.TileCrucible;
import io.tedsenft.gemstones.gui.crucible.ContainerCrucible;
import io.tedsenft.gemstones.gui.crucible.GuiCrucible;
import io.tedsenft.gemstones.base.GemContainer;
import io.tedsenft.gemstones.gui.gem_holder.GuiGemstoneHolder;
import io.tedsenft.gemstones.gui.gem_holder.ContainerGemstoneHolder;
import io.tedsenft.gemstones.blocks.pedestal.TilePedestal;
import io.tedsenft.gemstones.gui.pedestal.ContainerPedestal;
import io.tedsenft.gemstones.gui.pedestal.GuiPedestal;
import io.tedsenft.gemstones.reference.Guis;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

/**
 * Created by Ted on 2/21/2016.
 */
public class GuiHandler implements IGuiHandler {

    public static GuiHandler instance;



    /**
     * Returns a Server side Container to be displayed to the user.
     *
     * @param ID     The Gui ID Number
     * @param player The player viewing the Gui
     * @param world  The current world
     * @param x      X Position
     * @param y      Y Position
     * @param z      Z Position
     * @return A GuiScreen/Container to be displayed to the user, null if none.
     */
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

        switch(ID){
            case Guis.GEM_HOLDER:
                ItemStack held = player.getHeldItem(EnumHand.MAIN_HAND);
                if(held != null && held.getItem() instanceof GemContainer)
                    return new ContainerGemstoneHolder(player, held);

            case Guis.PEDESTAL:
                return new ContainerPedestal((TilePedestal) world.getTileEntity(new BlockPos(x,y,z)), player);

            case Guis.CRUCIBLE:
                return new ContainerCrucible(player, (TileCrucible) world.getTileEntity(new BlockPos(x,y,z)));
            default:
                return null;
        }
    }

    /**
     * Returns a Container to be displayed to the user. On the client side, this
     * needs to return a instance of GuiScreen On the server side, this needs to
     * return a instance of Container
     *
     * @param ID     The Gui ID Number
     * @param player The player viewing the Gui
     * @param world  The current world
     * @param x      X Position
     * @param y      Y Position
     * @param z      Z Position
     * @return A GuiScreen/Container to be displayed to the user, null if none.
     */
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch(ID){
            case Guis.GEM_HOLDER:
                ItemStack held = player.getHeldItem(EnumHand.MAIN_HAND);
                if(held != null && held.getItem() instanceof GemContainer)
                    return new GuiGemstoneHolder(player, held);

            case Guis.PEDESTAL:
                return new GuiPedestal((TilePedestal) world.getTileEntity(new BlockPos(x,y,z)), player);

            case Guis.CRUCIBLE:
                return new GuiCrucible(player, (TileCrucible) world.getTileEntity(new BlockPos(x,y,z)));

            default:
                return null;
        }
    }
}
