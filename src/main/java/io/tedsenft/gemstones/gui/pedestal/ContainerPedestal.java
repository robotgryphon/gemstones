package io.tedsenft.gemstones.gui.pedestal;

import io.tedsenft.gemstones.gui.GuiUtils;
import io.tedsenft.gemstones.blocks.pedestal.TilePedestal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

import javax.vecmath.Vector2f;

public class ContainerPedestal extends Container {

  public ContainerPedestal(TilePedestal tp, EntityPlayer p){
    SlotItemHandler sih = new SlotItemHandler(tp.gemHandler, 0, 10, 10);
    this.addSlotToContainer(sih);

    Slot[] inv = GuiUtils.createStandardInventory(p, new Vector2f(10, 80));
    for(Slot s : inv) this.addSlotToContainer(s);
  }

  @Override
  public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
    return null;
  }

  @Override
  public boolean canInteractWith(EntityPlayer playerIn) {
    // TODO: Create isWorking() and false if it is?
    return true;
  }
}
