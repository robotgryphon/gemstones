package io.tedsenft.gemstones.gui.pedestal;

import io.tedsenft.gemstones.blocks.pedestal.TilePedestal;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiPedestal extends GuiContainer {

  public GuiPedestal(TilePedestal tp, EntityPlayer player) {
    super(new ContainerPedestal(tp, player));

    this.xSize = 180;
    this.ySize = 200;
  }

  /**
   * Draws the background layer of this container (behind the items).
   *
   * @param partialTicks How far into the current tick the game is, with 0.0 being the start of the tick and 1.0 being
   *                     the end.
   * @param mouseX       Mouse x coordinate
   * @param mouseY       Mouse y coordinate
   */
  @Override
  protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
    mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MOD_ID, "textures/gui/wood_grain.png"));

    this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);

    for(int i = 0; i < inventorySlots.inventorySlots.size(); i++) {
      this.drawTexturedModalRect(guiLeft +  inventorySlots.getSlot(i).xDisplayPosition - 1, guiTop +  inventorySlots.getSlot(i).yDisplayPosition - 1, 0, this.ySize, 18, 18);
    }
  }
}
