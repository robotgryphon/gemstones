package io.tedsenft.gemstones.gui.crucible;

import io.tedsenft.gemstones.blocks.crucible.TileCrucible;
import io.tedsenft.gemstones.gui.GuiUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

import javax.vecmath.Vector2f;

public class ContainerCrucible extends Container {

  public ContainerCrucible(EntityPlayer player, TileCrucible crucible){

    // Add bucket in center of top
    Slot crucibleBucket = new SlotItemHandler(crucible.bucket, 0, 81, 36);
    this.addSlotToContainer(crucibleBucket);

    Slot crucibleMelting = new SlotItemHandler(crucible.bucket, 1, 81, 10);
    this.addSlotToContainer(crucibleMelting);

    Slot[] inv = GuiUtils.createStandardInventory(player, new Vector2f(10, 80));
    for(Slot s : inv) this.addSlotToContainer(s);
  }

  @Override
  public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
    return null;
  }

  @Override
  public boolean canInteractWith(EntityPlayer playerIn) {
    return true;
  }
}
