package io.tedsenft.gemstones.gui.gem_holder;

import io.tedsenft.gemstones.api.GemHolderInventory;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

/**
 * Created by Ted on 2/26/2016.
 */
public class SlotGem extends SlotItemHandler {
    public SlotGem(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
    }

    @Override
    public void onSlotChanged() {
        super.onSlotChanged();

        ((GemHolderInventory.GemInventory)  getItemHandler()).onSlotChanged(this.slotNumber);
    }
}
