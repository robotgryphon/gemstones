package io.tedsenft.gemstones.gui.gem_holder;

import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.gui.GuiUtils;
import io.tedsenft.gemstones.gui.SlotUnextractable;
import io.tedsenft.gemstones.api.GemHolderInventory;
import io.tedsenft.gemstones.items.Gemstone;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import javax.vecmath.Vector2f;

/**
 * Created by Ted on 2/21/2016.
 */
public class ContainerGemstoneHolder extends Container {

    private ItemStack gemHolder;
    public ContainerGemstoneHolder(EntityPlayer owner, ItemStack holder){
        this.gemHolder = holder;
        updateSlots(owner);
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
        int gems_end = ((IGemstoneHolder) gemHolder.getItem()).getNumberSlots() + 1;
        Slot s = this.inventorySlots.get(slotIndex);
        ItemStack originalBeforeMove = s.getStack();
        ItemStack afterMove = originalBeforeMove.copy();

        if(slotIndex < gems_end){
            // Moving a gem to inventory
            boolean canMoveToPlayer = this.mergeItemStack(afterMove, gems_end, inventorySlots.size(), false);
            if(canMoveToPlayer) s.putStack(null);
        } else {
            // Moving from inventory to gems
            if(originalBeforeMove.getItem() instanceof Gemstone){
                boolean canMoveFromPlayer = this.mergeItemStack(afterMove, 1, gems_end, false);
                if(canMoveFromPlayer) s.putStack(null);
            } else {
                return null;
            }
        }

        return null;
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();
    }

    public void updateSlots(EntityPlayer owner){
        this.inventorySlots.clear();

        GemHolderInventory wrapper = new GemHolderInventory(owner, gemHolder);

        int offset_gui = 8;

        this.addSlotToContainer(new SlotUnextractable(wrapper.gemHolder, 0, 18 + offset_gui, 18 + offset_gui));

        if (wrapper.gems.getSlots() > 0) {
            Vector2f pos = new Vector2f(3, 0);
            for(int slot = 0; slot < wrapper.gems.getSlots(); slot++){
                addSlotToContainer(new SlotGem(wrapper.gems, slot, ((int) pos.getX() * 18) + offset_gui, ((int) pos.getY() * 18) + offset_gui));
                pos.x++; if(pos.x == 9){ pos.x = 3; pos.y++; }
            }
        }

        Slot[] invSlots = GuiUtils.createStandardInventory(owner, new Vector2f(8, 80));
        for(Slot s : invSlots) this.addSlotToContainer(s);
    }
}
