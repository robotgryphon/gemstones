package io.tedsenft.gemstones.gui.gem_holder;

import io.tedsenft.gemstones.base.GemContainer;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Ted on 2/21/2016.
 */
public class GuiGemstoneHolder extends GuiContainer {

    private ItemStack holder;

    public GuiGemstoneHolder(EntityPlayer player, ItemStack gemHolder){
        super(new ContainerGemstoneHolder(player, gemHolder));
        this.holder = gemHolder;

        this.xSize = 180;
        this.ySize = 200;
    }

    /**
     * Args : renderPartialTicks, mouseX, mouseY
     *
     * @param partialTicks
     * @param mouseX
     * @param mouseY
     */
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        drawDefaultBackground();
        GlStateManager.color(1,1,1,1);

        mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MOD_ID, "textures/gui/wood_grain.png"));

        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);

        // Draw gem playeholders
        for(int i = 0; i < ((GemContainer) holder.getItem()).getNumberSlots() + 1; i++){
            if(!inventorySlots.getSlot(i).getHasStack())
                this.drawTexturedModalRect(guiLeft + inventorySlots.getSlot(i).xDisplayPosition - 1, guiTop + inventorySlots.getSlot(i).yDisplayPosition - 1, 18, this.ySize, 18, 18);
        }

        for(int i = ((GemContainer) holder.getItem()).getNumberSlots() + 1; i < inventorySlots.inventorySlots.size(); i++) {
            this.drawTexturedModalRect(guiLeft +  inventorySlots.getSlot(i).xDisplayPosition - 1, guiTop +  inventorySlots.getSlot(i).yDisplayPosition - 1, 0, this.ySize, 18, 18);
        }
    }
}
