package io.tedsenft.gemstones.creativetabs;

import io.tedsenft.gemstones.compat.baubles.Amulet;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by Ted on 2/23/2016.
 */
public class TabGemstones extends CreativeTabs {

    public static TabGemstones instance;

    public TabGemstones(){
        super(Reference.MOD_ID + ".tabs.default");
    }

    @Override
    public Item getTabIconItem() {
        return Amulet.instance(Amulet.class);
    }
}
