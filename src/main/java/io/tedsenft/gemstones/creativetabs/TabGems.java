package io.tedsenft.gemstones.creativetabs;

import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by Ted on 2/21/2016.
 */
public class TabGems extends CreativeTabs {

    public static TabGems instance;

    public TabGems() {
        super(Reference.MOD_ID + ".tab.gems");
    }

    @Override
    public Item getTabIconItem() {
        return Gemstone.instance(Gemstone.class);
    }
}
