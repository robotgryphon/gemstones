package io.tedsenft.gemstones.commands;

import com.google.common.collect.ImmutableList;
import io.tedsenft.gemstones.Gemstones;
import io.tedsenft.gemstones.api.IGemstoneAbility;
import io.tedsenft.gemstones.items.Gemstone;
import io.tedsenft.gemstones.abilities.AbilityRegistry;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;

import java.util.Collections;
import java.util.List;

public class CommandGems extends CommandBase {

  /**
   * Gets the name of the command
   */
  @Override
  public String getCommandName() {
    return "gemstones";
  }

  @Override
  public int getRequiredPermissionLevel() { return 2; }

  /**
   * Gets the usage string for the command.
   *
   * @param sender The command sender that executed the command
   */
  @Override
  public String getCommandUsage(ICommandSender sender) {
    return "/gems <command>";
  }

  @Override
  public List<String> getCommandAliases() {
    return ImmutableList.of("gems");
  }

  /**
   * Callback for when the command is executed
   *
   * @param server The Minecraft server instance
   * @param sender The source of the command invocation
   * @param args   The arguments that were passed
   */
  @Override
  public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
    if(sender.getCommandSenderEntity().worldObj.isRemote) return;
    if(!(sender.getCommandSenderEntity() instanceof  EntityPlayer)) return;
    if(args.length == 0) return;

    String firstArg = args[0].toLowerCase();
    if(firstArg.equals("give")){ }

    if(firstArg.equals("abilities")) {
      EntityPlayer ep = (EntityPlayer) sender.getCommandSenderEntity();
      ItemStack held = ep.getHeldItemMainhand();
      if (held == null) {
        ep.addChatComponentMessage(new TextComponentTranslation("gemstones.errors.no_held_gem"));
        return;
      }

      if (!(held.getItem() instanceof Gemstone)) {
        ep.addChatComponentMessage(new TextComponentTranslation("gemstones.errors.not_a_gem"));
        return;
      }

      Gemstone g = (Gemstone) held.getItem();
      if(args.length != 3) return;

      String abilityClassName = args[2].toLowerCase();
      if(!AbilityRegistry.isAbilityRegistered(abilityClassName)){
        ep.addChatComponentMessage(new TextComponentTranslation("gemstones.errors.invalid_ability"));
        return;
      }

      IGemstoneAbility ability = AbilityRegistry.getAbilityByMap(abilityClassName);
      if (args[1].toLowerCase().equals("add")) {
        boolean success = g.addAbility(held, ability);
        if(success)
          Gemstones.proxy.playSound(new ResourceLocation("random.successful_hit"), SoundCategory.MASTER, sender.getPosition(), 1f, 1f);
        else
          Gemstones.proxy.playSound(new ResourceLocation("random.fizz"), SoundCategory.MASTER, sender.getPosition(), 1f, 1f);

      } else if (args[1].toLowerCase().equals("remove")) {
        boolean success = g.removeAbility(held, ability);
        if(success)
          Gemstones.proxy.playSound(new ResourceLocation("random.successful_hit"), SoundCategory.MASTER, sender.getPosition(), 1f, 1f);
        else
          Gemstones.proxy.playSound(new ResourceLocation("random.fizz"), SoundCategory.MASTER, sender.getPosition(), 1f, 1f);
      } else {

      }
    }
  }

  @Override
  public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
    if(args.length == 1){
      return getListOfStringsMatchingLastWord(args, "abilities", "creategem");
    }

    if(args.length == 2 && args[0].toLowerCase().equals("abilities"))
      return getListOfStringsMatchingLastWord(args, "add", "remove");

    if(args.length == 3 && args[0].toLowerCase().equals("abilities"))
      return getListOfStringsMatchingLastWord(args, AbilityRegistry.getRegisteredClasses());

    return Collections.emptyList();
  }

  /**
   * Return whether the specified command parameter index is a username parameter.
   *
   * @param args  The arguments that were passed
   * @param index
   */
  @Override
  public boolean isUsernameIndex(String[] args, int index) {
    return false;
  }

  @Override
  public int compareTo(ICommand o) {
    return o.getCommandName().compareToIgnoreCase(this.getCommandName());
  }
}
