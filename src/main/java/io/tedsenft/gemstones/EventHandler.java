package io.tedsenft.gemstones;

import baubles.api.BaublesApi;
import io.tedsenft.gemstones.achievements.AchList;
import io.tedsenft.gemstones.api.IGemstoneHolder;
import io.tedsenft.gemstones.blocks.pedestal.BlockPedestal;
import io.tedsenft.gemstones.utils.LogHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

/**
 * Created by Ted on 2/21/2016.
 */
public class EventHandler {

    public static EventHandler instance;

    @SubscribeEvent
    public void playerLoaded(net.minecraftforge.event.entity.player.PlayerEvent.LoadFromFile ev){
        LogHelper.log("Player loaded from file: " + ev.getPlayerUUID());
    }

    @SubscribeEvent
    public void onCrafting(PlayerEvent.ItemCraftedEvent ev){
        if(ev.crafting.getItem().getRegistryName().equals(BlockPedestal.instance(BlockPedestal.class).getRegistryName())){
            ev.player.addStat(AchList.gettingStarted, 1);
        }
    }

    @SubscribeEvent
    public void onAttacked(LivingAttackEvent evt){
        Entity el = evt.getEntity();
        if(el instanceof EntityPlayer){
            EntityPlayer player = (EntityPlayer) el;
            IInventory baubs = BaublesApi.getBaubles(player);

            if (baubs.getSizeInventory() > 0) {
                // We have baubles
                int baubsSize = baubs.getSizeInventory();
                for(int baubSlot = 0; baubSlot < baubsSize; baubSlot++){
                    if(baubs.getStackInSlot(baubSlot) == null)
                        continue;

                    ItemStack is = baubs.getStackInSlot(baubSlot);
                    if(is.getItem() instanceof IGemstoneHolder){
                        ((IGemstoneHolder) is.getItem()).handleDamage(is, evt);
                    }
                }
            }
        }
    }
}
